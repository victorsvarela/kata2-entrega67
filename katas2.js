// comece a criar a sua função add na linha abaixo
const add = (numberAdd1, numberAdd2) => {
    let soma = numberAdd1 + numberAdd2
    return soma
}

// descomente a linha seguinte para testar sua função
console.assert(add(3, 5) === 8, 'A função add não está funcionando como esperado');

// comece a criar a sua função multiply na linha abaixo
const multiply = (numberMultiply1, numberMultiply2) => {
    let soma = 0

    for(let i = 0; i < numberMultiply1; i++){
        soma = add(numberMultiply2, soma)
    }
    return soma
}

// descomente a linha seguinte para testar sua função
console.assert(multiply(4, 6) === 24, 'A função multiply não está funcionando como esperado');


// comece a criar a sua função power na linha abaixo
const power = (numberPower1, numberPower2) => {
    let soma = 1
    
    for(let i = 0; i < numberPower2; i++){
        soma = multiply(soma, numberPower1)
    }
    return soma
}

// descomente a linha seguinte para testar sua função
console.assert(power(3, 4) === 81, 'A função power não está funcionando como esperado');


// comece a criar a sua função factorial na linha abaixo
const factorial = (numFatorial) => {
    let soma = 1

    for(let i = 1; i <= numFatorial; i++){
        soma = multiply(soma,i)
    }
    return soma
}

// descomente a linha seguinte para testar sua função
console.assert(factorial(5) === 120, 'A função factorial não está funcionando como esperado');

/**
 * BONUS (aviso: o grau de dificuldade é bem maior !!!)
 */

// crie a função fibonacci
//const fibonacci = (numFibonacci) => {
//}

// descomente a linha seguinte para testar sua função
//console.assert(fibonacci(8) === 13, 'A função fibonacci não está funcionando como esperado');
